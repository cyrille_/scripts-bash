#!/bin/bash

######################################################################
# Script qui permet de switcher entre la connexion 
# internet et virtuelle créée par KVM (interface br0).
# Le problème étant, qu'une fois br0 activée, 
# il n'est plus possible d'accéder à internet 
# via le WIFI : les deux sont incompatibles. 
######################################################################

# si option --state alors on affiche l'état de la connexion

if [ "$1" == "--state"  ];then
	
	ip -c a | grep br0 > fic

file=fic
word="br0"

cmd=$(grep -ci "$word" $file)

	if [ "$cmd" != "0" ]; then
          
       	 echo ""
       	 echo "		~*!-  Vous n'êtes pas connecté à internet = interface br0 active  -!*~"
	else	
       	 echo ""
       	 echo "		~*!-  Vous êtes connecté à internet -!*~"
	fi


# si option --internet

elif [ "$1" == "--internet" ];then

# on vérifie que le fichier de configuration netplan est configuré avec internet

sortie_diff_1=$(diff /etc/netplan/01-network-manager-all.yaml /etc/netplan/internet)


# test du résultat de sortie de la commande "diff" sur les 2 fichiers 
	
	if test -z "$sortie_diff_1"
	
	then

# Si ce sont les mêmes alors on affiche

		echo "La configuration pour internet est déjà active"
	
# Si ce n'est pas le cas alors on copie le contenu du fichier "internet" dans le fhichier de config netplan	
	
	else		

		cat /etc/netplan/internet > /etc/netplan/01-network-manager-all.yaml 
		
		echo "La configuration a été changée, veuillez redémarrer votre ordinateur"
		
	fi


# si option --virtuel

elif [ "$1" == "--virtual" ];then

# on vérifie que le fichier de configuration netplan est configuré avec la config virtuelle

sortie_diff_2=$(diff /etc/netplan/01-network-manager-all.yaml /etc/netplan/virtual)

# test du résultat de sortie de la commande "diff" sur les 2 fichiers 
	
	if test -z "$sortie_diff_2"
	
	then

# Si ce sont les mêmes alors on affiche

	echo "La configuration virtuelle est déjà active"
	
# Si ce n'est pas le cas alors on copie le contenu du fichier "virtual" dans le fhichier de config netplan	
	
	else		

		cat /etc/netplan/virtual > /etc/netplan/01-network-manager-all.yaml 
		
		echo "La configuration a été changée, veuillez redémarrer votre ordinateur"
		
	fi


# si aucune option affichage de l'aide

else

	echo "
--etat : Savoir l'état de la connexion
--internet : passer sur la connexion internet
--virtuel : passer sur la connexion virtuelle
"

fi


#Contenu des fichiers "virtual" et "internet" :

<< internet_file
# Let NetworkManager manage all devices on this system
network:
version: 2
renderer: NetworkManager
internet_file

<< virtual_file
network:
  ethernets:
    enp0s31f6:
      dhcp4: false
      dhcp6: false
  bridges:
    br0:
      interfaces: [ enp0s31f6 ]
      addresses: [X.X.X.X/24]
      gateway4: X.X.X.X
      mtu: 1500
      nameservers:
        addresses: [X.X.X.X,Y.Y.Y.Y]
      parameters:
        stp: true
        forward-delay: 4
      dhcp4: no
      dhcp6: no
  version: 2

virtual_file
