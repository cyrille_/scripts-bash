#!/bin/bash

# Script qui sert à créer des pages .html et a y insérer le contenu d'un template, en personnalisant les les variables avec le nom du fichier. 

# Création de fichiers .html, correspondant aux fichiers .txt déjà présents

for file in *.txt
do
touch "${file%.*}.html"
done

# Dans chaque fichier .html, on copie le contenu du fichier "template" qui se trouve à la racine

cat template | tee *.html

# Dans le contenu des fichiers copiés,  on remplace le terme "item" par le nom du fichier lui-même

for name in *.html
do
var=$(echo "${name%.*}") 
sed -i "s/item/$var/g" *$var.html
done

exit
